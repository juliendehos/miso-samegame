let pkgs = (import ./nix/nixpkgs.nix ).pkgs;
in {
  ghc = pkgs.haskell.packages.ghc865.callCabal2nix "app" ./. {};
  ghcjs = pkgs.haskell.packages.ghcjs.callCabal2nix "app" ./. {};
}

