module Samegame.Samegame where

import qualified Data.Vector as V

data Move = Move
    { _i :: Int
    , _j :: Int
    , _color :: Int
    , _group :: Int
    , _eval :: Int
    } deriving (Eq, Show)

data Samegame = Samegame
    { _nbI :: Int
    , _nbJ :: Int
    , _nbColors :: Int
    , _score :: Int
    , _nbGroups :: Int
    , _dataColors :: V.Vector Int   -- nbI*nbJ
    , _dataGroups :: V.Vector Int   -- nbI*nbJ
    , _groupSizes :: V.Vector Int   -- <= nbI*nbJ
    , _lastIs :: V.Vector Int       -- nbJ
    , _lastJ :: Int
    , _moves :: [Move]
    } deriving (Eq, Show)

create :: Int -> Int -> Int -> [Int] -> Samegame
create nbI nbJ nbColors _colors = updateGroupsAndMoves sg'
    where sg' = Samegame nbI nbJ nbColors 0 0
                    (V.fromList (take (nbI*nbJ) _colors))
                    V.empty 
                    V.empty 
                    (V.replicate nbJ (nbI-1))
                    (nbJ-1)
                    []

ind :: Samegame -> Int -> Int -> Int
ind sg i j = i * _nbJ sg + j

isValid :: Samegame -> (Int, Int) -> Bool
isValid sg (i, j) = i >= 0 && i < _nbI sg && j >= 0 && j < _nbJ sg 
                        && getDataColor sg i j /= (-1)

isPlayable :: Samegame -> (Int, Int) -> Bool
isPlayable sg (i, j) = (_groupSizes sg V.! getDataGroup sg i j) > 1

getDataColor :: Samegame -> Int -> Int -> Int
getDataColor sg i j = _dataColors sg V.! ind sg i j

getDataGroup :: Samegame -> Int -> Int -> Int
getDataGroup sg i j = _dataGroups sg V.! ind sg i j

isTerminated :: Samegame -> Bool
isTerminated sg = null (_moves sg)

updateGroupsAndMoves :: Samegame -> Samegame
updateGroupsAndMoves = updateMoves . updateGroups

getPositions :: Samegame -> [(Int, Int)]
getPositions sg = [ (i, j) | j <- [0 .. _lastJ sg]
                           , i <- [0 .. (_lastIs sg V.! j)] ]

getPlayablePos :: Samegame -> [(Int, Int)]
getPlayablePos sg = filter (isPlayable sg) (getPositions sg)

updateGroups :: Samegame -> Samegame
updateGroups sg = foldl updateGroup sg' positions
    where sg' = sg { _dataGroups = V.replicate fullsize (-1)
                   , _groupSizes = V.replicate fullsize 0
                   , _nbGroups = 0 }
          fullsize = _nbI sg * _nbJ sg
          positions = getPositions sg'

updateGroup :: Samegame -> (Int, Int) -> Samegame
updateGroup sg (i, j)
    | (_dataGroups sg V.! ind sg i j) /= (-1) = sg
    | otherwise = let color = getDataColor sg i j
                      iGroup = _nbGroups sg
                      sg' = sg { _nbGroups = iGroup + 1 }
                  in buildGroup sg' color iGroup [(i, j)]

buildGroup :: Samegame -> Int -> Int -> [(Int, Int)] -> Samegame
buildGroup sg _color _iGroup [] = sg
buildGroup sg color iGroup ((i, j):ijs) = 
    if getDataColor sg i j /= color || getDataGroup sg i j /= (-1)
    then buildGroup sg color iGroup ijs
    else let groupSize' = 1 + _groupSizes sg V.! iGroup
             sg' = sg { _dataGroups = _dataGroups sg V.// [(ind sg i j, iGroup)]
                      , _groupSizes = _groupSizes sg V.// [(iGroup, groupSize')] }
             nextIjs = filter (isValid sg) [(i-1,j),(i+1,j),(i,j-1),(i,j+1)]
             ijs' = nextIjs ++ ijs
         in buildGroup sg' color iGroup ijs'

updateMoves :: Samegame -> Samegame
updateMoves sg = sg { _moves = moves }
    where moves = map (buildMove sg) (getPlayablePos sg)

buildMove :: Samegame -> (Int, Int) -> Move
buildMove sg (i, j) = Move i j color group eval
    where color = getDataColor sg i j
          group = getDataGroup sg i j
          groupSize2 = (_groupSizes sg V.! group) - 2
          eval = groupSize2 * groupSize2

play :: Samegame -> Int -> Int -> Samegame -- TODO
play sg i j = case filter (\m -> _i m == i && _j m == j) (_moves sg) of
    [move] -> playMove sg move
    _ -> sg

playMove :: Samegame -> Move -> Samegame
playMove sg0 (Move _ _ _ group eval) = updateGroupsAndMoves sg3
    where ijs = filter (\(i, j) -> getDataGroup sg0 i j == group) (getPositions sg0)
          sg1 = foldl contractI sg0 (reverse ijs)
          js = filter (\j -> (_lastIs sg1 V.! j) == -1) (reverse [0 .. _lastJ sg1])
          sg2 = foldl contractJ sg1 js
          sg3 = sg2 { _score = _score sg0 + eval }

contractI :: Samegame -> (Int, Int) -> Samegame
contractI sg (i0, j0) = sg { _dataColors = dataColors', _lastIs = lastIs' }
    where lastIs = _lastIs sg
          lastI = lastIs V.! j0
          lastI' = lastI - 1
          lastIs' = lastIs V.// [(j0, lastI')]
          is = [i0 .. lastI']
          vs = map (\i -> getDataColor sg (i+1) j0) is ++ [-1]
          ks = map (\i -> ind sg i j0) (is ++ [lastI])
          dataColors' = _dataColors sg V.// zip ks vs

contractJ :: Samegame -> Int -> Samegame
contractJ sg j0 = sg { _dataColors = dataColors', _lastIs = lastIs', _lastJ = lastJ' }
    where lastJ = _lastJ sg
          lastJ' = lastJ - 1

          ijs = [ (i, j) | i<-[0 .. _nbI sg - 1], j<-[j0 .. lastJ] ]
          fGet (i, j) = if j == lastJ then -1 else getDataColor sg i (j+1)
          vs = map fGet ijs
          ks = map (uncurry (ind sg)) ijs
          dataColors' = _dataColors sg V.// zip ks vs

          last_ks = [j0 .. lastJ]
          last_fGet j = if j == lastJ then -1 else _lastIs sg V.! (j+1)
          last_vs = map last_fGet last_ks
          lastIs' = _lastIs sg V.// zip last_ks last_vs

-- TODO use map/filter/zip from the vector module ?

