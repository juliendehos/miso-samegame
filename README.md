# miso-samegame

[Try online !](https://juliendehos.gitlab.io/miso-samegame/)

## dev

- use binary cache:

```
cachix use miso-haskell
```

- build & run native apps:

```
make simple
make cli
```

- build web app: 

```
make client
firefox public/index.html
```

- run tests using cabal:

```
make test
```

## release

```
make clean
nix-build nix/release.nix
```

then see the `result` folder

