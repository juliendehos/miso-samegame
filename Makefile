test:
	nix-shell -A ghc.env --run "cabal --builddir=dist-ghc --config-file=config/ghc.config test"

simple:
	nix-shell -A ghc.env --run "cabal --builddir=dist-ghc --config-file=config/ghc.config run app-simple"

cli:
	nix-shell -A ghc.env --run "cabal --builddir=dist-ghc --config-file=config/ghc.config run app-cli"

client:
	nix-shell -A ghcjs.env --run "cabal --builddir=dist-ghcjs --config-file=config/ghcjs.config build app-client"
	ln -sf ../`find dist-ghcjs/ -name all.js` public/

clean:
	rm -rf dist-* result* public/all.js 

