module Samegame.SamegameSpec (main, spec) where

import qualified Samegame.Samegame as SG

import qualified Data.Vector as V
import Test.Hspec

main :: IO ()
main = hspec spec

sg1, sg1_00, sg1_00_12 :: SG.Samegame
sg1 = SG.create 2 4 3 
        [ 0, 0, 1, 1
        , 0, 2, 0, 1 ]
sg1_00 = SG.play sg1 0 0
sg1_00_12 = SG.play sg1_00 1 2

sg2 :: SG.Samegame
sg2 = SG.create 4 6 3 
        [ 0, 0, 2, 1, 1, 0
        , 0, 2, 0, 1, 0, 2
        , 1, 1, 1, 1, 0, 1
        , 0, 2, 0, 2, 2, 2 ]

spec :: Spec
spec = do

    describe "create" $ do
        it "_nbI" $ SG._nbI sg1 `shouldBe` 2
        it "_nbJ" $ SG._nbJ sg1 `shouldBe` 4
        it "_nbColors" $ SG._nbColors sg1 `shouldBe` 3
        it "_score" $ SG._score sg1 `shouldBe` 0
        it "_nbGroups" $ SG._score sg1 `shouldBe` 0
        it "_dataColors" $ V.toList (SG._dataColors sg1)
                            `shouldBe` [ 0, 0, 1, 1
                                       , 0, 2, 0, 1 ]
        it "_dataGroups" $ V.toList (SG._dataGroups sg1)
                            `shouldBe` [ 0, 0, 2, 2
                                       , 0, 1, 3, 2 ]
        it "_groupSizes" $ V.toList (SG._groupSizes sg1)
                            `shouldBe` [ 3, 1, 3, 1, 0, 0, 0, 0 ]
        it "_lastIs" $ V.toList (SG._lastIs sg1) `shouldBe` [1, 1, 1, 1]
        it "_lastJ" $ SG._lastJ sg1 `shouldBe` 3
        it "_moves" $ SG._moves sg1 `shouldBe` 
            [ SG.Move 0 0 0 0 1
            , SG.Move 1 0 0 0 1
            , SG.Move 0 1 0 0 1
            , SG.Move 0 2 1 2 1
            , SG.Move 0 3 1 2 1
            , SG.Move 1 3 1 2 1 ]

    describe "ind" $ do
        it "1 3" $ SG.ind sg1 1 3 `shouldBe` 7
        it "0 1" $ SG.ind sg1 0 1 `shouldBe` 1
        it "1 0" $ SG.ind sg1 1 0 `shouldBe` 4
        it "1 2" $ SG.ind sg1 1 2 `shouldBe` 6

    describe "isValid" $ do
        it "1 3" $ SG.isValid sg1 (1, 3) `shouldBe` True
        it "0 0" $ SG.isValid sg1 (0, 0) `shouldBe` True
        it "1 2" $ SG.isValid sg1 (1, 2) `shouldBe` True
        it "2 2" $ SG.isValid sg1 (2, 2) `shouldBe` False
        it "1 4" $ SG.isValid sg1 (1, 4) `shouldBe` False
        it "-1 2" $ SG.isValid sg1 (-1, 2) `shouldBe` False
        it "1 -2" $ SG.isValid sg1 (1, -2) `shouldBe` False

    describe "getDataColor" $ do
        it "0 0" $ SG.getDataColor sg1 0 0 `shouldBe` 0
        it "1 0" $ SG.getDataColor sg1 1 0 `shouldBe` 0
        it "0 1" $ SG.getDataColor sg1 0 1 `shouldBe` 0
        it "1 1" $ SG.getDataColor sg1 1 1 `shouldBe` 2
        it "0 2" $ SG.getDataColor sg1 0 2 `shouldBe` 1
        it "1 2" $ SG.getDataColor sg1 1 2 `shouldBe` 0
        it "0 3" $ SG.getDataColor sg1 0 3 `shouldBe` 1
        it "1 3" $ SG.getDataColor sg1 1 3 `shouldBe` 1

    describe "getDataGroup" $ do
        it "0 0" $ SG.getDataGroup sg1 0 0 `shouldBe` 0
        it "1 0" $ SG.getDataGroup sg1 1 0 `shouldBe` 0
        it "0 1" $ SG.getDataGroup sg1 0 1 `shouldBe` 0
        it "1 1" $ SG.getDataGroup sg1 1 1 `shouldBe` 1
        it "0 2" $ SG.getDataGroup sg1 0 2 `shouldBe` 2
        it "1 2" $ SG.getDataGroup sg1 1 2 `shouldBe` 3
        it "0 3" $ SG.getDataGroup sg1 0 3 `shouldBe` 2
        it "1 3" $ SG.getDataGroup sg1 1 3 `shouldBe` 2

    describe "getPositions" $ do
        it "sg1" $ SG.getPositions sg1 
            `shouldBe` [(0,0),(1,0),(0,1),(1,1),(0,2),(1,2),(0,3),(1,3)]
        it "sg1_00" $ SG.getPositions sg1_00 
            `shouldBe` [(0,0),(0,1),(1,1),(0,2),(1,2)]
        it "sg1_00_12" $ SG.getPositions sg1_00_12 `shouldBe` [(0,0),(0,1)]

    describe "getPlayablePos" $ do
        it "sg1" $ SG.getPlayablePos sg1
            `shouldBe` [(0,0),(1,0),(0,1),(0,2),(0,3),(1,3)]
        it "sg1_00" $ SG.getPlayablePos sg1_00 `shouldBe` [(0,1),(0,2),(1,2)]
        it "sg1_00_12" $ SG.getPlayablePos sg1_00_12 `shouldBe` []

    describe "isTerminated" $ do
        it "sg1" $ SG.isTerminated sg1 `shouldBe` False
        it "sg1_00" $ SG.isTerminated sg1_00 `shouldBe` False
        it "sg1_00_12" $ SG.isTerminated sg1_00_12 `shouldBe` True

    describe "contractI" $ do
        it "1a" $ V.toList (SG._dataColors (SG.contractI sg2 (1, 2))) 
            `shouldBe` [ 0, 0, 2, 1, 1, 0
                       , 0, 2, 1, 1, 0, 2
                       , 1, 1, 0, 1, 0, 1
                       , 0, 2, -1, 2, 2, 2 ]
        it "1b" $ V.toList (SG._lastIs (SG.contractI sg2 (1, 2))) 
            `shouldBe` [ 3, 3, 2, 3, 3, 3 ]
        it "2a" $ V.toList (SG._dataColors (SG.contractI sg2 (0, 1))) 
            `shouldBe` [ 0, 2, 2, 1, 1, 0
                       , 0, 1, 0, 1, 0, 2
                       , 1, 2, 1, 1, 0, 1
                       , 0, -1, 0, 2, 2, 2 ]
        it "2b" $ V.toList (SG._lastIs (SG.contractI sg2 (0, 1))) 
            `shouldBe` [ 3, 2, 3, 3, 3, 3 ]
        it "3a" $ V.toList (SG._dataColors (SG.contractI sg2 (3, 4))) 
            `shouldBe` [ 0, 0, 2, 1, 1, 0
                       , 0, 2, 0, 1, 0, 2
                       , 1, 1, 1, 1, 0, 1
                       , 0, 2, 0, 2, -1, 2 ]
        it "3b" $ V.toList (SG._lastIs (SG.contractI sg2 (3, 4))) 
            `shouldBe` [ 3, 3, 3, 3, 2, 3 ]

    describe "contractJ" $ do
        it "1" $ V.toList (SG._dataColors (SG.contractJ sg2 1)) 
            `shouldBe` [ 0, 2, 1, 1, 0, -1
                       , 0, 0, 1, 0, 2, -1
                       , 1, 1, 1, 0, 1, -1
                       , 0, 0, 2, 2, 2, -1 ]

        it "2" $ V.toList (SG._dataColors (SG.contractJ sg2 5)) 
            `shouldBe` [ 0, 0, 2, 1, 1, -1
                       , 0, 2, 0, 1, 0, -1
                       , 1, 1, 1, 1, 0, -1
                       , 0, 2, 0, 2, 2, -1 ]

        let sg2_i = SG.contractI sg2 (1, 2)
        let sg2_i_j = SG.contractJ sg2_i 1
        it "3 i j" $ V.toList (SG._lastIs sg2_i_j)
            `shouldBe` [ 3, 2, 3, 3, 3, -1 ]

    describe "play" $ do
        it "sg1_00 colors" $ V.toList (SG._dataColors sg1_00) 
            `shouldBe` [  2, 1, 1, -1
                       , -1, 0, 1, -1 ]
        it "sg1_00 groups" $ V.toList (SG._dataGroups sg1_00) 
            `shouldBe` [ 0,  1, 1, -1
                       , -1, 2, 1, -1 ]
        it "sg1_00 lastIs" $ V.toList (SG._lastIs sg1_00) 
            `shouldBe` [ 0, 1, 1, -1 ]

        it "sg1_00_12 colors" $ V.toList (SG._dataColors sg1_00_12) 
            `shouldBe` [  2,  0, -1, -1
                       , -1, -1, -1, -1 ]
        it "sg1_00_12 groups" $ V.toList (SG._dataGroups sg1_00_12) 
            `shouldBe` [ 0, 1, -1, -1
                       , -1, -1, -1, -1 ]
        it "sg1_00_12 lastIs" $ V.toList (SG._lastIs sg1_00_12) 
            `shouldBe` [ 0, 0, -1, -1 ]

