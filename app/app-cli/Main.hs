import qualified Samegame.Samegame as SG

import qualified Data.Vector as V
import System.Random

nbI, nbJ, nbColors :: Int
(nbI, nbJ, nbColors) = (4, 6, 3)

display :: Int -> [Int] -> IO ()
display _ [] = return ()
display n xxs = do
    let (x, xs) = splitAt n xxs
    putStrLn $ unwords $ map (\c -> if c == (-1) then "." else show c) x
    display n xs

run :: SG.Samegame -> IO ()
run sg = do
    putStrLn ""
    display nbJ $ V.toList $ SG._dataColors sg
    putStrLn $ "score: " ++ show (SG._score sg)
    if SG.isTerminated sg
    then putStrLn "terminated"
    else do 
        ij <- map read . words <$> getLine
        case ij of
            [i, j] -> run (SG.play sg i j)
            _ -> run sg

main :: IO ()
main = do
    rands <- randomRs (0, nbColors-1) <$> newStdGen
    let sg = SG.create nbI nbJ nbColors rands
    run sg

