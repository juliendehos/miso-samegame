import qualified Samegame.Samegame as SG

import qualified Data.Vector as V
-- import System.Random

nbI, nbJ, nbColors :: Int
(nbI, nbJ, nbColors) = (4, 6, 3)

listToMatrix :: Int -> [Int] -> [[Int]]
listToMatrix _ [] = []
listToMatrix n xxs = x : listToMatrix n xs
    where (x, xs) = splitAt n xxs

display :: SG.Samegame -> IO ()
display sgf = do
    print sgf
    putStrLn "\ncolors:"
    mapM_ print $ listToMatrix (SG._nbJ sgf) $ V.toList $ SG._dataColors sgf
    putStrLn "\ngroups:"
    mapM_ print $ listToMatrix (SG._nbJ sgf) $ V.toList $ SG._dataGroups sgf
    putStrLn "\nmoves:"
    mapM_ print $ SG._moves sgf
    putStrLn $ "\nscore: " ++ show (SG._score sgf)

main :: IO ()
main = do

    let sg1 = SG.create 2 4 3 
                [ 0, 0, 1, 1
                , 0, 2, 0, 1 ]
    display sg1
    let sg1_00 = SG.play sg1 0 0
    display sg1_00
    let sg1_00_12 = SG.play sg1_00 1 2
    display sg1_00_12


{-

    let sg0 = SG.create nbI nbJ nbColors 
                [ 0, 0, 0, 0, 1, 1
                , 2, 1, 2, 2, 0, 1
                , 0, 2, 2, 1, 1, 0
                , 2, 1, 2, 1, 0, 2 ]
    display sg0
    let sg1 = SG.play sg0 1 2
    display sg1
    let sg2 = SG.play sg1 0 0
    display sg2
    let sg3 = SG.play sg2 1 1
    display sg3

-}


{-
    let sg0 = SG.create nbI nbJ nbColors 
                [1,2,1,2,1,0
                ,0,1,0,2,0,2
                ,1,1,1,0,0,1
                ,2,2,0,1,0,2]

    -- rands <- randomRs (0, nbColors-1) <$> newStdGen
    -- let sg0 = SG.create nbI nbJ nbColors rands

    -- print sg0
    putStrLn "\ncolors:"
    mapM_ print $ listToMatrix nbJ $ V.toList $ SG._dataColors sg0
    putStrLn "\ngroups:"
    mapM_ print $ listToMatrix nbJ $ V.toList $ SG._dataGroups sg0
    -- putStrLn "\nmoves:"
    -- mapM_ print $ SG._moves sg0
    putStrLn $ "\nscore: " ++ show (SG._score sg0)

    let sg1 = SG.play sg0 2 4

    -- print sg1
    putStrLn "\ncolors:"
    mapM_ print $ listToMatrix nbJ $ V.toList $ SG._dataColors sg1
    putStrLn "\ngroups:"
    mapM_ print $ listToMatrix nbJ $ V.toList $ SG._dataGroups sg1
    -- putStrLn "\nmoves:"
    -- mapM_ print $ SG._moves sg1
    putStrLn $ "\nscore: " ++ show (SG._score sg1)

    let sg2 = SG.play (SG.play sg0 1 1) 1 1

    -- print sg2
    putStrLn "\ncolors:"
    mapM_ print $ listToMatrix nbJ $ V.toList $ SG._dataColors sg2
    putStrLn "\ngroups:"
    mapM_ print $ listToMatrix nbJ $ V.toList $ SG._dataGroups sg2
    -- putStrLn "\nmoves:"
    -- mapM_ print $ SG._moves sg2
    putStrLn $ "\nscore: " ++ show (SG._score sg2)
-}

