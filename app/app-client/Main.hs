{-# LANGUAGE OverloadedStrings #-}

import qualified Constants as C
import qualified Samegame.Samegame as SG

import qualified JavaScript.Web.Canvas as JSC
import qualified Miso.String as MS

import Control.Monad (guard)
import Data.Map (singleton)
import Miso
import System.Random (newStdGen, randomRs)

-------------------------------------------------------------------------------
-- main types & functions
-------------------------------------------------------------------------------

data Model = Model
    { _rands :: [Int]
    , _sg :: SG.Samegame
    , _ij :: (Int, Int)
    } deriving (Eq)

data Action
    = ActionNone
    | ActionDisplay
    | ActionNewGame
    | ActionPlayMove
    | ActionXY (Int, Int)
    | ActionIJ (Int, Int)

useRands :: [Int] -> ([Int], [Int])
useRands rs = let (rs1, rs2) = splitAt (C.nbI*C.nbJ) rs in (rs1, rs2++rs1)

main :: IO ()
main = do
    rands <- take C.nbRands . randomRs (0, C.nbColors-1) <$> newStdGen
    let (rands1, rands2) = useRands rands
    let game0 = SG.create C.nbI C.nbJ C.nbColors rands1
    startApp App
        { initialAction = ActionDisplay
        , update        = updateModel
        , view          = viewModel
        , model         = Model rands2 game0 (0, 0)
        , subs          = [ mouseSub ActionXY ]
        , events        = defaultEvents
        , mountPoint    = Nothing
        , logLevel      = Off
        }

-------------------------------------------------------------------------------
-- update
-------------------------------------------------------------------------------

updateModel :: Action -> Model -> Effect Action Model

updateModel ActionNone m = noEff m

updateModel ActionDisplay m = m <# (drawSamegame (_sg m) >> pure ActionNone)

updateModel ActionNewGame m = m' <# pure ActionDisplay
    where (rands1, rands2) = useRands (_rands m)
          sg' = SG.create C.nbI C.nbJ C.nbColors rands1
          m' = m { _rands = rands2, _sg = sg' }

updateModel (ActionXY xy) m = m <# (ActionIJ <$> xy2ij xy)

updateModel (ActionIJ ij) m = noEff m { _ij = ij }

updateModel ActionPlayMove m = m' <# pure ActionDisplay
    where (i, j) = _ij m
          sg' = SG.play (_sg m) i j
          m' = m { _sg = sg' }

-------------------------------------------------------------------------------
-- view
-------------------------------------------------------------------------------

viewModel :: Model -> View Action
viewModel (Model _ sg _) = div_ []
    [ h1_ [] [ text "miso-samegame" ]
    , p_ [] [ button_ [onClick ActionNewGame] [text "new game"] ]
    , p_ [] [ text (MS.ms $ "score: " ++ show (SG._score sg))]
    , p_ [] [ canvas_ [ id_ "mycanvas"
                      , width_ (MS.ms $ SG._nbJ sg * C.cellSize)
                      , height_ (MS.ms $ SG._nbI sg * C.cellSize)
                      , style_  (singleton "border" "1px solid black")
                      , onMouseUp ActionPlayMove
                      ] []
            ]
    , p_ []
         [ a_ [ href_ "https://en.wikipedia.org/wiki/SameGame"]
              [ text "SameGame on Wikipedia" ]
         , text " / "
         , a_ [ href_ "https://gitlab.com/juliendehos/miso-samegame"]
              [ text "source code" ]
         , text " / "
         , a_ [ href_ "https://juliendehos.gitlab.io/miso-samegame"]
              [ text "demo" ]
         ]
    ]

drawDisc :: JSC.Context -> SG.Samegame -> (Int, Int) -> IO ()
drawDisc ctx sg (i, j) = do
    let color = SG.getDataColor sg i j
    guard (color >= 0 && color < length C.colors)
    let (r, g, b) = C.colors !! color
    let delta = C.cellSize `div` 2
    let x = fromIntegral (j * C.cellSize + delta)
    let y = fromIntegral (i * C.cellSize + delta)
    let radius = 0.45 * C.cellSizeD
    JSC.beginPath ctx
    JSC.lineWidth 1 ctx
    JSC.strokeStyle 0 0 0 255 ctx
    JSC.fillStyle r g b 255 ctx
    JSC.arc x y radius 0 (2 * pi) True ctx
    JSC.fill ctx
    JSC.stroke ctx

drawSamegame :: SG.Samegame -> IO ()
drawSamegame sg = do
    let bgColor = if SG.isTerminated sg then 200 else 100
    let positions = SG.getPositions sg
    ctx <- jsGetCtx
    JSC.clearRect 0 0 C.widthD C.heightD ctx
    JSC.fillStyle bgColor bgColor bgColor 255 ctx
    JSC.fillRect 0 0 C.widthD C.heightD ctx
    jsConsoleLog (MS.ms $ show positions)
    mapM_ (drawDisc ctx sg) positions
    JSC.stroke ctx

xy2ij :: (Int, Int) -> IO (Int, Int)
xy2ij (x, y) = do
    left <- jsRectLeft
    top <- jsRectTop
    let i = (y - top) `div` C.cellSize
    let j = (x - left) `div` C.cellSize
    return (i, j)

----------------------------------------------------------------------
-- JavaScript FFI
----------------------------------------------------------------------

foreign import javascript unsafe "$r = mycanvas.getContext('2d');"
    jsGetCtx :: IO JSC.Context

foreign import javascript unsafe "console.log($1);"
    jsConsoleLog :: MS.MisoString -> IO ()

foreign import javascript unsafe "$r = mycanvas.getBoundingClientRect().left;"
    jsRectLeft :: IO Int

foreign import javascript unsafe "$r = mycanvas.getBoundingClientRect().top;"
    jsRectTop :: IO Int

