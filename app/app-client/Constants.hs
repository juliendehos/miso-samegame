module Constants where

nbI :: Int
nbI = 12

nbJ :: Int
nbJ = 16

nbColors :: Int
nbColors = 5

nbRands :: Int
nbRands = nbI*nbJ*100 + 3

cellSize :: Int
cellSize = 40

cellSizeD :: Double
cellSizeD = fromIntegral cellSize

widthD :: Double
widthD = fromIntegral (nbJ * cellSize)

heightD :: Double
heightD = fromIntegral (nbI * cellSize)

colors :: [(Int, Int, Int)]
colors =
    [ (255,   0,   0)  -- red
    , (  0, 255,   0)  -- green
    , (  0,   0, 255)  -- blue
    , (255, 255,   0)  -- yellow
    , (  0, 255, 255)  -- cyan
    , (255,   0, 255)  -- magenta
    , (255, 255, 255)  -- white
    , (  0,   0,   0)  -- black
    ]

