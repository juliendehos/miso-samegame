let

  pkgs = (import ./nixpkgs.nix).pkgs ;
  app-src = ../. ;

  app-ghc = pkgs.haskell.packages.ghc865.callCabal2nix "app" ../. {};
  app-ghcjs = pkgs.haskell.packages.ghcjs.callCabal2nix "app" ../. {};

in

  # todo test
  pkgs.runCommand "app" { inherit app-ghc app-ghcjs; } ''
    mkdir -p $out/{bin,public}
    cp ${app-ghc}/bin/app-cli $out/bin
    ${pkgs.closurecompiler}/bin/closure-compiler ${app-ghcjs}/bin/app-client.jsexe/all.js > $out/public/all.js
    cp ${app-src}/public/* $out/public/
  ''

